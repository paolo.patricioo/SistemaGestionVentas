<?php

namespace SistemaGestionVentas\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use Carbon\Carbon;

class dashboardController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    //Cumplimiento de metas
    public function index()
    {
        //total de articulos por categoria
        $slot1=DB::table('detalle_ingreso as di')->join('articulo as a','a.id','di.idarticulo')->where('a.idcategoria','=','11')->where('a.estado','=','Activo')->sum('cantidad');
        $slot2=DB::table('detalle_ingreso as di')->join('articulo as a','a.id','di.idarticulo')->where('a.idcategoria','=','12')->where('a.estado','=','Activo')->sum('cantidad');
        $slot3=DB::table('detalle_ingreso as di')->join('articulo as a','a.id','di.idarticulo')->where('a.idcategoria','=','13')->where('a.estado','=','Activo')->sum('cantidad');
        $slot4=DB::table('detalle_ingreso as di')->join('articulo as a','a.id','di.idarticulo')->where('a.idcategoria','=','14')->where('a.estado','=','Activo')->sum('cantidad');

        //venta forecast de articulos
        $ventas1=DB::table('detalle_venta as dv')->join('articulo as a','a.id','dv.idarticulo')->where('a.idcategoria','=','11')->where('a.estado','=','Activo')->sum('cantidad');
        $ventas2=DB::table('detalle_venta as dv')->join('articulo as a','a.id','dv.idarticulo')->where('a.idcategoria','=','12')->where('a.estado','=','Activo')->sum('cantidad');
        $ventas3=DB::table('detalle_venta as dv')->join('articulo as a','a.id','dv.idarticulo')->where('a.idcategoria','=','13')->where('a.estado','=','Activo')->sum('cantidad');
        $ventas4=DB::table('detalle_venta as dv')->join('articulo as a','a.id','dv.idarticulo')->where('a.idcategoria','=','14')->where('a.estado','=','Activo')->sum('cantidad');

        //barra de progreso
        if($slot1==0){
            $progreso1=0;
        }else{
            $progreso1= ($ventas1 * 100) / $slot1;
        }

        if($slot2==0){
            $progreso2=0;
        }else{
            $progreso2= ($ventas2 * 100) / $slot2;
        }

        if($slot3==0){
            $progreso3=0;
        }else{
            $progreso3= ($ventas3 * 100) / $slot3;
        }

        if($slot4==0){
            $progreso4=0;
        }else{
            $progreso4= ($ventas4 * 100) / $slot4;
        }

        //ingresos totales
        $ingTotal=DB::table('venta as v')->where('estado','=','A')->sum('total_venta');

        // Fechas y querys para el resumen general
        $dt = Carbon::now();
        $mesActual= Carbon::now();
        $mesActual= $dt->month;
        if($mesActual < 10){
            $mesAct= '0'.$mesActual;
        }else{
            $mesAct= $mesActual;
        }
        $mesAnterior= $dt->subMonths(1);
        $mesAnterior= $dt->month;
        if($mesAnterior < 10){
            $mesTxt= '0'.$mesAnterior;
        }else{
            $mesTxt= $mesAnterior;
        }

        // ingresos totales mensuales
        $ingMesPasado=DB::table('venta as v')->where('estado','=','A')->where('fecha_hora','LIKE','%-'.$mesTxt.'-%')->sum('total_venta');
        $ingMesActual=DB::table('venta as v')->where('estado','=','A')->where('fecha_hora','LIKE','%-'.$mesAct.'-%')->sum('total_venta');
        if($ingMesPasado == 0){
            $ingMesPasado=1;
        }

        // Costos totales
        $costoTotal=DB::table('detalle_ingreso as di')
        ->join('ingreso as i','i.id','di.idingreso')
        ->select(DB::raw('sum(di.cantidad * di.precio_compra) as total'))
        ->where('estado','=','A')
        ->get();

        $cosMesPasado=DB::table('detalle_ingreso as di')
        ->join('ingreso as i','i.id','di.idingreso')
        ->select(DB::raw('sum(di.cantidad * di.precio_compra) as total'))
        ->where('estado','=','A')
        ->where('i.fecha_hora','LIKE','%-'.$mesTxt.'-%')
        ->get();

        $cosMesActual=DB::table('detalle_ingreso as di')
        ->join('ingreso as i','i.id','di.idingreso')
        ->select(DB::raw('sum(di.cantidad * di.precio_compra) as total'))
        ->where('estado','=','A')
        ->where('fecha_hora','LIKE','%-'.$mesAct.'-%')
        ->get();
        
        foreach($cosMesPasado as $cosMesPasado_aux){
            if($cosMesPasado_aux->total == null){
                $costoMesPasado=0.000001;
            }else{
                $costoMesPasado=$cosMesPasado_aux->total;
            }
        }

        foreach($cosMesActual as $cosMesActual_aux){
                $costoActual=$cosMesActual_aux->total;
        }

        foreach($costoTotal as $cosTotal_aux){
                $costoTotalGral=$cosTotal_aux->total;
        }

        $costoTotalMesActual=$costoActual-$costoMesPasado;

        //total de articulos vendidos 
        $totArt=DB::table('detalle_ingreso as di')->join('articulo as a','a.id','di.idarticulo')->where('a.estado','=','Activo')->sum('cantidad');
        $artVendidos=DB::table('detalle_venta as dv')->join('articulo as a','a.id','dv.idarticulo')->where('a.estado','=','Activo')->sum('cantidad');

        // porcentaje de variacion de ingresos, costos, ganancias y total de productos vendidos
        $varIngTotales= ($ingMesActual - $ingMesPasado)*100 / $ingMesPasado;
        if($varIngTotales > 100){
            $varIngTotales=100;
        }

        $varCosTotales= ($costoActual-$costoMesPasado)*100 / $costoMesPasado;
        if($varCosTotales > 100){
            $varCosTotales=100;
        }
        if($varCosTotales < 100){
            $varCosTotales=$varCosTotales*-1;
        }
        $varGanTotales=0;
        if($costoTotalGral==0){
            $costoTotalGral=1;
            $varGanTotales= (($ingTotal * 100) / $costoTotalGral) - 100;
        }else{
            if($varGanTotales > 100){
                $varGanTotales=100;
            }
        }

        $varProdTotales=0;
        if($totArt==0){
            $totArt=1;
        }else{
            $varProdTotales= ($artVendidos*100) / $totArt;
            if($varProdTotales > 100){
                $varProdTotales=100;
            }
        }
        

        /*
        flag flechas de tendencia
        - verde = green
        - amarillo = yellow
        - rojo = red
        */
        if($varIngTotales == 0){
            $flagTendencia1="yellow";
            $flagCaret1="left";
        }elseif ($varIngTotales > 0 ) {
            $flagTendencia1="green";
            $flagCaret1="up";
        }else{
            $flagTendencia1="red";
            $flagCaret1="down";
        }

        if($varCosTotales == 0){
            $flagTendencia2="yellow";
            $flagCaret2="left";
        }elseif ($varCosTotales < 0 ) {
            $varCosTotales=$varCosTotales*-1;
            $flagTendencia2="green";
            $flagCaret2="up";
        }else{
            $flagTendencia2="red";
            $flagCaret2="down";
        }

        if($varGanTotales == 0){
            $flagTendencia3="yellow";
            $flagCaret3="left";
        }elseif ($varGanTotales > 0 ) {
            $flagTendencia3="green";
            $flagCaret3="up";
        }else{
            $flagTendencia3="red";
            $flagCaret3="down";
        }

        if($varProdTotales < 40){
            $flagTendencia4="red";
        }elseif ($varProdTotales > 75 ) {
            $flagTendencia4="green";
        }else{
            $flagTendencia4="yellow";
        }

        //ganancia total
        $ganTotal=$ingTotal-$costoTotalGral;


        //Ingresos Hoy
        $hoy= Carbon::today();
        $hoyFormat= $hoy->toFormattedDateString(); 
        $hoyDia= $hoy->format('d'); 
        $hoyMes= $hoy->format('m'); 
        $IngHoy=DB::table('venta as v')->where('fecha_hora','=',$hoy)->sum('total_venta');

        //acceso a detalle
        $ingHoyDet=DB::table('venta as v')
        ->join('detalle_venta as dv','v.idventa','dv.idventa')
        ->join('articulo as a','a.id','dv.idarticulo')
        ->select('v.idventa as idventa','v.num_comprobante as num_comprobante','a.nombre as nombre','dv.cantidad as cantidad','dv.descuento as descuento','dv.precio_venta as precio_venta',DB::raw('(dv.cantidad * dv.precio_venta) - dv.descuento as total_venta'))
        ->where('v.estado','=','A')
        ->where(DB::raw('DAY(v.fecha_hora)'),'=',$hoyDia)
        ->get();

        //Articulos Vendidos Hoy
        $artHoy=DB::table('venta as v')
        ->join('detalle_venta as dv','v.idventa','dv.idventa')
        ->where('fecha_hora','=',$hoy)->sum('cantidad');

        //Ingresos "mes" 2018
        $IngMes=DB::table('venta as v')->where('fecha_hora','LIKE','%-'.$mesAct.'-%')->sum('total_venta');

        //Costos Totales "mes" 2018
        $cosMesActual=DB::table('detalle_ingreso as di')
        ->join('ingreso as i','i.id','di.idingreso')
        ->select(DB::raw('sum(di.cantidad * di.precio_compra) as total'))
        ->where('estado','=','A')
        ->where('fecha_hora','LIKE','%-'.$mesAct.'-%')
        ->get();

        //acceso a detalle
        $ingMesDet=DB::table('venta as v')
        ->join('detalle_venta as dv','v.idventa','dv.idventa')
        ->join('articulo as a','a.id','dv.idarticulo')
        ->select('v.idventa as idventa','v.num_comprobante as num_comprobante','a.nombre as nombre','dv.cantidad as cantidad','dv.descuento as descuento','dv.precio_venta as precio_venta',DB::raw('(dv.cantidad * dv.precio_venta) - dv.descuento as total_venta'))
        ->where('v.estado','=','A')
        ->where(DB::raw('MONTH(v.fecha_hora)'),'=',$hoyMes)
        ->get();

        $mesActFormat=$dt->addMonths(1)->format('F Y');

        //Grafico de barras
        $mesesDelAnio= collect(['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']);
        $costoTotalMensual= DB::table('meses as m')
                            ->leftJoin('ingreso as i','m.num_mes','=','i.mes_ingreso')
                            ->leftJoin('detalle_ingreso as di','i.id','=','di.idingreso')
                            ->select('m.mestxt as mes',DB::raw('sum(di.cantidad * di.precio_compra) as compra'))
                            ->groupby('m.mestxt')
                            ->orderby('m.idmeses')
                            ->get();
        $IngresoTotalMensual= DB::table('meses as m')
                            ->leftJoin('venta as v','m.num_mes','=','v.mes_venta')
                            ->select('m.mestxt as mes',DB::raw('sum(total_venta) as venta'))
                            ->groupby('m.mestxt')
                            ->orderby('m.idmeses')
                            ->get();


        //tabla stock inicial
        $tablaStockInicial=DB::table('detalle_ingreso as di')
        ->join('articulo as art','di.idarticulo','art.id')
        ->join('categoria as c','art.idcategoria','c.id')
        ->select('di.idarticulo as id','c.nombre as nombre_cat','art.codigo as codigo','art.nombre as nombre',DB::raw('sum(di.cantidad) as cantidad'))
        ->where('art.codigo','<>','0')
        ->where('art.estado','=','Activo')
        ->orderby('cantidad','desc')
        ->groupby('di.idarticulo','c.nombre','art.codigo','art.nombre')
        ->get();

        $pieStockInicial=DB::table('articulo as art')
        ->join('categoria as c','art.idcategoria','c.id')
        ->join('detalle_ingreso as di','art.id','di.idarticulo')
        ->select('di.idarticulo as id','c.nombre as nombre_cat','art.codigo as codigo','art.nombre as nombre',DB::raw('sum(di.cantidad) as cantidad'))
        ->where('art.estado','=','Activo')
        ->where('art.codigo','<>','0')
        ->orderby('cantidad','desc')
        ->groupby('di.idarticulo','c.nombre','art.codigo','art.nombre')
        ->get();

        $pieStockInicialTop=DB::table('articulo as art')
        ->join('categoria as c','art.idcategoria','c.id')
        ->join('detalle_ingreso as di','art.id','di.idarticulo')
        ->select('di.idarticulo as id','c.nombre as nombre_cat','art.codigo as codigo','art.nombre as nombre',DB::raw('sum(di.cantidad) as cantidad'))
        ->where('art.estado','=','Activo')
        ->where('art.codigo','<>','0')
        ->orderby('cantidad','desc')
        ->groupby('di.idarticulo','c.nombre','art.codigo','art.nombre')
        ->paginate(5);

        //tabla densidad de ventas

        $tablaDensidadVentas= DB::table('articulo as art')
        ->join('categoria as c','art.idcategoria','c.id')
        ->join('detalle_venta as dv','art.id','dv.idarticulo')
        ->join('venta as v','v.idventa','dv.idventa')
        ->select('art.id as id','c.nombre as nombre_cat','art.codigo as codigo','art.nombre as nombre',DB::raw('sum(dv.cantidad) as ventas'))
        ->where('v.estado','=','A')
        ->groupby('art.id','c.nombre','art.codigo','art.nombre')
        ->orderby('ventas','desc')
        ->get(); 

        $pieDensidadVentas= DB::table('articulo as art')
        ->join('categoria as c','art.idcategoria','c.id')
        ->join('detalle_venta as dv','art.id','dv.idarticulo')
        ->join('venta as v','v.idventa','dv.idventa')
        ->select('art.id as id','c.nombre as nombre_cat','art.codigo as codigo','art.nombre as nombre',DB::raw('sum(dv.cantidad) as ventas'))
        ->where('v.estado','=','A')
        ->groupby('art.id','c.nombre','art.codigo','art.nombre')
        ->orderby('ventas','desc')
        ->paginate(5);

        //Ultimas compras de articulos
        $articulos=DB::table('articulo as a')
        ->join('categoria as c','a.idcategoria','=','c.id')
        ->join('detalle_ingreso as di','a.id','di.idarticulo')
        ->join('ingreso as i','i.id','di.idingreso')
        ->select('a.id','a.nombre','a.codigo','a.stock','di.cantidad','c.nombre as categoria','a.descripcion','a.imagen','a.estado','i.fecha_hora')
        ->orderBy('i.id','desc')
        ->paginate(3);

        //Felipe no tocar hacia arriba
        //Articulos vendidos hoy
        $ventasArticulosHoy=DB::table('articulo as art')
        ->join('categoria as cat','art.idcategoria','=','cat.id')
        ->join('detalle_venta as dev','art.id','=','dev.idarticulo')
        ->join('venta as v','v.idventa','=','dev.idventa')
        ->select('v.num_comprobante','v.idventa','art.id','cat.nombre as nombre_c','art.nombre as nombre_a','dev.cantidad')
        ->where(DB::raw('DAY(v.fecha_hora)'),'=',$hoyDia)
        ->orderBy('art.id','desc')
        ->paginate(10);

        //Costos mensual
        $CostoArticulo=DB::table('ingreso as i')
        ->join('detalle_ingreso as di','i.id','di.idingreso')
        ->join('articulo as a','a.id','di.idarticulo')
        ->select('i.num_comprobante','i.id','a.nombre','di.cantidad','di.precio_compra',DB::raw('(di.cantidad * di.precio_compra) as total_compra'))
        ->where(DB::raw('MONTH(i.fecha_hora)'),'=',$hoyMes)
        ->get();

        return view('administracion.index',[ 
            'slot1_var'=>$slot1,
            'slot2_var'=>$slot2,
            'slot3_var'=>$slot3,
            'slot4_var'=>$slot4,
            'ventas1_var'=>$ventas1,
            'ventas2_var'=>$ventas2,
            'ventas3_var'=>$ventas3,
            'ventas4_var'=>$ventas4,
            'progreso1_var'=>$progreso1,
            'progreso2_var'=>$progreso2,
            'progreso3_var'=>$progreso3,
            'progreso4_var'=>$progreso4,
            'ingTotal_var'=>$ingTotal,
            'cosTotal_var'=>$costoTotalGral,
            'porcentaje1_var'=>$varIngTotales,
            'porcentaje2_var'=>$varCosTotales,
            'porcentaje3_var'=>$varGanTotales,
            'porcentaje4_var'=>$varProdTotales,
            'tendencia1_var'=>$flagTendencia1,
            'tendencia2_var'=>$flagTendencia2,
            'tendencia3_var'=>$flagTendencia3,
            'tendencia4_var'=>$flagTendencia4,
            'flagCaret1_var'=>$flagCaret1,
            'flagCaret2_var'=>$flagCaret2,
            'flagCaret3_var'=>$flagCaret3,
            'hoy_var'=>$hoyFormat,
            'mes_var'=>$mesActFormat,
            'ingHoy_var'=>$IngHoy,
            'artHoy_var'=>$artHoy,
            'ingMes_var'=>$IngMes,
            'ingMes_var'=>$IngMes,
            'cosMes_var'=>$costoTotalMesActual,
            'ganTotal_var'=>$ganTotal,
            'artVendidos_var'=>$artVendidos,
            'costoTotal_var'=>$costoTotalMensual,
            'ingresoTotal_var'=>$IngresoTotalMensual,
            'tablaStockInicial_var'=>$tablaStockInicial,
            'pieStockInicial_var'=>$pieStockInicial,
            'pieStockInicial5_var'=>$pieStockInicialTop,
            'tablaDensidadVentas_var'=>$tablaDensidadVentas,
            'pieDensidadVentas_var'=>$pieDensidadVentas,
            'articulos_var'=>$articulos,
            'meses_var'=>$mesesDelAnio,
            'ingDetHoy_var'=>$ingHoyDet,
            'ingDetMes_var'=>$ingMesDet,
            'ventasArticulosHoy_var'=>$ventasArticulosHoy,
            'CostoArticulo_var'=>$CostoArticulo,
            ]);
    }
}
