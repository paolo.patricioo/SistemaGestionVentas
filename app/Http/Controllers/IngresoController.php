<?php

namespace SistemaGestionVentas\Http\Controllers;

use Illuminate\Http\Request;

use SistemaGestionVentas\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use SistemaGestionVentas\Http\Requests\IngresoFormRequest;
use SistemaGestionVentas\Ingreso;
use SistemaGestionVentas\DetalleIngreso;
use DB;

use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;

class IngresoController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){
        if ($request){
            $query=trim($request->get('searchText'));
            $ingresos=DB::table('ingreso as i')
            ->join('persona as p','i.idproveedor','=','p.id')
            ->join('detalle_ingreso as di','i.id','=','di.idingreso')
            ->select('i.id','i.fecha_hora','p.nombre','i.serie_comprobante','i.tipo_comprobante','i.num_comprobante','i.impuesto','i.estado', DB::raw('sum(di.cantidad*precio_compra) as total'))
            ->where('i.num_comprobante','LIKE','%'.$query.'%')
            ->orderBy('i.id','desc')
            ->groupby('i.id','i.fecha_hora','p.nombre','i.serie_comprobante','i.tipo_comprobante','i.num_comprobante','i.impuesto','i.estado')
            ->paginate(7);
            return view('compras.ingreso.index',["ingresos"=>$ingresos,"searchText"=>$query]);
        }
    }

    public function create(){
        $personas=DB::table('persona')->where('tipo_persona','=','Proveedor')->get();
        $articulos=DB::table('articulo as art')
        ->select(DB::raw('CONCAT(art.codigo, " ", art.nombre) as articulo'),'art.id')
        ->where('art.estado','=','Activo')
        ->get();
        return view("compras.ingreso.create", ["personas"=>$personas, "articulos"=>$articulos]);
    }   

    public function store(IngresoFormRequest $request){


        try{

            $initIdProveedor = $request->get('idproveedor');
            $initTipoComprobante = $request->get('tipo_comprobante');
            $initIdArticulo =  $request->get('idarticulo');

            DB::beginTransaction();

            $ingreso=new Ingreso;
            \Debugbar::info($ingreso); 
            $ingreso->idproveedor = $initIdProveedor;
            $ingreso->tipo_comprobante = $initTipoComprobante;
            $ingreso->serie_comprobante=$request->get('serie_comprobante');
            $ingreso->num_comprobante=$request->get('num_comprobante');

            $mytime = Carbon::now();
            $hora = $mytime->toDateString();
            $mes =  $mytime->month;
            $ingreso->fecha_hora = $hora;
            $ingreso->impuesto="1";
            $ingreso->estado="A";
            $ingreso->mes_ingreso=$mes;
            $ingreso->save();
 
                
            $idarticulo = $initIdArticulo;
            $cantidad = $request->get('cantidad');
            $precio_compra = $request->get('precio_compra');
            $precio_venta = $request->get('precio_venta');

            $cont = 0;
            while ($cont < count($idarticulo)){

                $idaux=DB::table('ingreso as i')
                ->max('id');

                $detalle = new DetalleIngreso;                
                $detalle->idingreso = $idaux;
                $detalle->idarticulo= $idarticulo[$cont];
                $detalle->cantidad= $cantidad[$cont];
                $detalle->precio_compra= $precio_compra[$cont];
                $detalle->precio_venta= $precio_venta[$cont];
                $detalle->save();
                $cont++;     
                \Debugbar::info($detalle);    
            } 

            DB::commit();

        }catch(\Exception $e){
            echo $e->getMessage();
            DB::rollback();
        }
        

        return Redirect::to('compras/ingreso');
    }
    
    public function show($id){
        $ingreso=DB::table('ingreso as i')
            ->join('persona as p','i.idproveedor','=','p.id')
            ->join('detalle_ingreso as di','i.id','=','di.idingreso')
            ->select('i.id','i.fecha_hora','p.nombre','i.serie_comprobante','i.tipo_comprobante','i.num_comprobante','i.impuesto','i.estado', DB::raw('sum(di.cantidad*precio_compra) as total'))
            ->where('i.id','=',$id)
            ->groupby('i.id','i.fecha_hora','p.nombre','i.serie_comprobante','i.tipo_comprobante','i.num_comprobante','i.impuesto','i.estado')
            ->first();

        $detalle=DB::table('detalle_ingreso as d')
        ->join('articulo as a','d.idarticulo','=','a.id')
        ->select('a.nombre as articulo','d.cantidad','d.precio_compra','d.precio_venta')
        ->where('d.idingreso','=',$id)->get();    

        return view("compras.ingreso.show",["ingreso"=>$ingreso,"detalles"=>$detalle]);
    }

    public function destroy($id)
    {
        $ingreso=Ingreso::findOrFail($id);
        $ingreso->estado='C';
        $ingreso->Update();
        return Redirect::to('compras/ingreso');
    }
}
