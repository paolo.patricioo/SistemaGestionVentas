<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.1.0
    </div>
    <strong>Copyright &copy; 2018-2025 <a href="www.incanatoit.com">OneTweed Ltda.</a>.</strong> All rights reserved.
</footer>