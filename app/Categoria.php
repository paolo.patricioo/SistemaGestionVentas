<?php

namespace SistemaGestionVentas;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table='categoria';
    protected $primarykey='id';
    public $timestamps=false;


    protected $fillable =[
        'nombre',
        'descripcion',
        'condicion'
    ];
    protected $guarded =[

    ];
}
