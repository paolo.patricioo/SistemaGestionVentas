<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/js/app.min.js') }}" type="text/javascript"></script>

<!-- libreria simpleweather pal tiempo -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.simpleWeather/3.1.0/jquery.simpleWeather.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->

<!-- AdminLTE App 
<script src="{{ asset('/js/ownjs/live.js') }}" type="text/javascript"></script> -->

<!-- ChartJS -->
<script src="{{ asset('/js/ownjs/Chart.js') }}"></script> 

<!-- DataTables -->
<script src="{{ asset('/js/ownjs/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/ownjs/dataTables.bootstrap.min.js') }}"></script>