<?php
 
namespace SistemaGestionVentas\Http\Controllers;

use Illuminate\Http\Request;

use SistemaGestionVentas\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use SistemaGestionVentas\Http\Requests\VentaFormRequest;
use SistemaGestionVentas\Venta;
use SistemaGestionVentas\DetalleVenta;
use DB;

use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;

class VentaController extends Controller
{
    public function index(Request $request){
        if ($request){
            $query=trim($request->get('searchText'));
            $ventas=DB::table('venta as v')
            ->join('persona as p','v.idcliente','=','p.id')
            ->join('detalle_venta as dv','v.idventa','=','dv.idventa')
            ->select('v.idventa','v.fecha_hora','p.nombre','v.serie_comprobante','v.tipo_comprobante','v.num_comprobante','v.impuesto','v.estado','v.total_venta')
            ->where('v.num_comprobante','LIKE','%'.$query.'%')
            ->orderBy('v.idventa','desc')
            ->groupby('v.idventa','v.fecha_hora','p.nombre','v.serie_comprobante','v.tipo_comprobante','v.num_comprobante','v.impuesto','v.estado','v.total_venta')
            ->paginate(7);
            return view('ventas.venta.index',["ventas"=>$ventas,"searchText"=>$query]);
        }
    }

    public function create(){
        $personas=DB::table('persona')->where('tipo_persona','=','Cliente')->get();
        $articulos=DB::table('articulo as art')
        ->join('detalle_ingreso as di','art.id','=','di.idarticulo')
        ->select(DB::raw('CONCAT(art.codigo, " ", art.nombre) as articulo'),'art.id','art.stock',DB::raw('avg(di.precio_venta) as precio_promedio'))
        ->where('art.estado','=','Activo')
        ->where('art.stock','>','0')
        ->groupby('articulo','art.id','art.stock')
        ->get();
        return view("ventas.venta.create", ["personas"=>$personas, "articulos"=>$articulos]);
    }   

    public function store(VentaFormRequest $request){
        try{

            $initIdcliente = $request->get('idcliente');
            $initTipoComprobante = $request->get('tipo_comprobante');
            $initIdArticulo =  $request->get('idarticulo');
            $initTotalVenta =  $request->get('total_venta');

            DB::beginTransaction();

            $venta=new venta; 
            $venta->idcliente = $initIdcliente;
            $venta->tipo_comprobante = $initTipoComprobante;
            $venta->serie_comprobante=$request->get('serie_comprobante');
            $venta->num_comprobante=$request->get('num_comprobante');
            $venta->total_venta= $initTotalVenta; 

            $mytime = Carbon::now();
            $hora = $mytime;//->toDateString();
            $mes = $mytime->month;
            $venta->fecha_hora = $hora;
            $venta->impuesto="1";
            $venta->estado="A";
            $venta->mes_venta=$mes;
            $venta->save(); 
            
            
            $idarticulo = $initIdArticulo;
            $cantidad = $request->get('cantidad');
            $descuento = $request->get('descuento');
            $precio_venta = $request->get('precio_venta');
            
            $cont = 0;

            while ($cont < count($idarticulo)){               

                $idaux=DB::table('venta as v')
                ->max('idventa');

                $detalle = new DetalleVenta();
                $detalle->idventa = $idaux;
                $detalle->idarticulo= $idarticulo[$cont];
                $detalle->cantidad= $cantidad[$cont];
                $detalle->descuento= $descuento[$cont];
                $detalle->precio_venta= $precio_venta[$cont];
                $detalle->save();
                $cont++;
            }  

            DB::commit();

        }catch(\Exception $e){
            error_log($e);
            DB::rollback();
        }
        

        return Redirect::to('ventas/venta');
    }
    
    public function show($id){
        $venta=DB::table('venta as v')
            ->join('persona as p','v.idcliente','=','p.id')
            ->join('detalle_venta as dv','v.idventa','=','dv.idventa')
            ->select('v.idventa','v.fecha_hora','p.nombre','v.serie_comprobante','v.tipo_comprobante','v.num_comprobante','v.impuesto','v.estado', 'v.total_venta')
            ->where('v.idventa','=',$id)
            //->groupby('i.id','i.fecha_hora','p.nombre','i.serie_comprobante','i.tipo_comprobante','i.num_comprobante','i.impuesto','i.estado')
            ->first();

        $detalle=DB::table('detalle_venta as v')
        ->join('articulo as a','v.idarticulo','=','a.id')
        ->select('a.nombre as articulo','v.cantidad','v.descuento','v.precio_venta')
        ->where('v.idventa','=',$id)->get();    

        return view("ventas.venta.show",["venta"=>$venta,"detalles"=>$detalle]);
    }

    public function destroy($id)
    { 
        $venta=DB::table('venta as v')
        ->select('v.idventa as id,v.estado as estado')
        ->where('v.idventa','=',$id)->get();
        $venta->estado='C';
        $venta->Save();
        return Redirect::to('ventas/venta');
    }
}
