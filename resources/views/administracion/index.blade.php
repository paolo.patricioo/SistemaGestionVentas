<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	@extends ('administracion.app') @section ('main-content')
	<section class="content">
		<!-- Paolo 08 - 2 - 2018 = agrego recapitulacion financiera mensual -->
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Vista de metas mensuales</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" type="button"><i class="fa fa-minus"></i></button>
							<div class="btn-group">
								<button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-wrench"></i></button>
								<ul class="dropdown-menu" role="menu">
									<li>
										<a href="#">Action</a>
									</li>
									<li>
										<a href="#">Another action</a>
									</li>
									<li>
										<a href="#">Something else here</a>
									</li>
									<li class="divider"></li>
									<li>
										<a href="#">Separated link</a>
									</li>
								</ul>
							</div><button class="btn btn-box-tool" data-widget="remove" type="button"><i class="fa fa-times"></i></button>
						</div>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-8">
								
							<!-- BAR CHART -->
								<div class="box-header with-border">
									<div class="box-body">
										<div class="chart">
											<canvas id="barChart" style="height:230px"></canvas>
										</div>
									</div>
									<!-- /.box-body -->
								</div>
								<!-- /.box -->

							</div><!-- /.col -->
							<div class="col-md-4">
								<p class="text-center"><strong>Cumplimiento de metas Feb. 2018 (ventas)</strong></p>
								<div class="progress-group">
									<span class="progress-text">Cereales y Galletas</span> <span class="progress-number"><b>@isset ($ventas1_var) {{$ventas1_var}} @endisset</b>/@isset ($slot1_var) {{$slot1_var}} @endisset </span>
									<div class="progress sm">
										<div class="progress-bar progress-bar-aqua" style="width: @isset ($progreso1_var) {{$progreso1_var}}% @endisset"></div>
									</div>
								</div><!-- /.progress-group -->
								<div class="progress-group">
									<span class="progress-text">Bebidas, Jugos y Lácteos</span> <span class="progress-number"><b>@isset ($ventas2_var) {{$ventas2_var}} @endisset</b>/@isset ($slot1_var) {{$slot2_var}} @endisset</span>
									<div class="progress sm">
										<div class="progress-bar progress-bar-aqua" style="width: @isset ($progreso2_var) {{$progreso2_var}}% @endisset"></div>
									</div>
								</div><!-- /.progress-group -->
								<div class="progress-group">
									<span class="progress-text">Comida rápida, Frituras</span> <span class="progress-number"><b>@isset ($ventas3_var) {{$ventas3_var}} @endisset</b>/@isset ($slot1_var) {{$slot3_var}} @endisset</span>
									<div class="progress sm">
										<div class="progress-bar progress-bar-aqua" style="width: @isset ($progreso3_var) {{$progreso3_var}}% @endisset"></div>
									</div>
								</div><!-- /.progress-group -->
								<div class="progress-group">
									<span class="progress-text">Golosinas y Dulces</span> <span class="progress-number"><b>@isset ($ventas4_var) {{$ventas4_var}} @endisset</b>/@isset ($slot1_var) {{$slot4_var}} @endisset</span>
									<div class="progress sm">
										<div class="progress-bar progress-bar-aqua" style="width: @isset ($progreso4_var) {{$progreso4_var}}% @endisset"></div>
									</div>
								</div><!-- /.progress-group -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- ./box-body -->
					<div class="box-footer">
						<div class="row">
							<div class="col-sm-3 col-xs-6">
								<div class="description-block border-right">
									<span class="description-percentage text-{{$tendencia1_var}}"><i class="fa fa-caret-{{$flagCaret1_var}}"></i> @isset ($porcentaje1_var) {{$porcentaje1_var}}% @endisset</span>
									<h5 class="description-header">$ @isset ($ingTotal_var)  {{ number_format($ingTotal_var,1) }} @endisset</h5><span class="description-text">INGRESOS TOTALES</span>
								</div><!-- /.description-block -->
							</div><!-- /.col -->
							<div class="col-sm-3 col-xs-6">
								<div class="description-block border-right">
									<span class="description-percentage text-{{$tendencia2_var}}"><i class="fa fa-caret-{{$flagCaret2_var}}"></i> @isset ($cosTotal_var)  {{ number_format($porcentaje2_var,0) }}%  @endisset </span>
									<h5 class="description-header">$ @isset ($cosTotal_var)  {{ number_format($cosTotal_var,1) }}  @endisset</h5><span class="description-text">COSTOS TOTAL</span>
								</div><!-- /.description-block -->
							</div><!-- /.col -->
							<div class="col-sm-3 col-xs-6">
								<div class="description-block border-right">
									<span class="description-percentage text-{{$tendencia3_var}}"><i class="fa fa-caret-{{$flagCaret3_var}}"></i> @isset ($porcentaje3_var)  {{ number_format($porcentaje3_var,0) }}%  @endisset</span>
									<h5 class="description-header">$ @isset ($ganTotal_var)  {{ number_format($ganTotal_var,1) }}  @endisset</h5><span class="description-text">GANANCIA TOTAL</span>
								</div><!-- /.description-block -->
							</div><!-- /.col -->
							<div class="col-sm-3 col-xs-6">
								<div class="description-block">
									<span class="description-percentage text-{{$tendencia4_var}}"><i class=""></i> @isset ($porcentaje4_var)  {{ number_format($porcentaje4_var,2) }}%  @endisset</span>
									<h5 class="description-header">@isset ($artVendidos_var)  {{ $artVendidos_var }}  @endisset</h5><span class="description-text">TOTAL DE PRODUCTOS VENDIDOS</span>
								</div><!-- /.description-block -->
							</div>
						</div><!-- /.row -->
					</div><!-- /.box-footer -->
				</div><!-- /.box -->
			</div><!-- /.col -->
		</div><!-- /.row -->
		<!-- /fin recapitulacion financiera mensual -->
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-lg-3 col-md-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<h3>@isset ($ingHoy_var)  {{ number_format($ingHoy_var,0) }}  @endisset<sup style="font-size: 20px">$</sup></h3>
						<p>Ingresos</p>
						<p>@isset ($hoy_var)  {{ $hoy_var }}  @endisset</p>
					</div>
					<div class="icon">
						<i class="ion ion-arrow-graph-up-left"></i>
					</div><a class="small-box-footer" data-toggle="modal" data-target="#modal-success" href="#">Detalles <i class="fa fa-arrow-circle-right"></i></a>
					<div class="modal modal-success fade" id="modal-success">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">Ingresos por productos de hoy</h4>
								</div>
								<div class="modal-body">
									<table class="table" id="example-modalIngHoy">
										<thead>
											<tr>
												<th>N° Comprobante</th>
												<th>ID Venta</th>
												<th>Nombre</th>
												<th>Cantidad</th>
												<th>Precio Venta</th>
												<th>Descuento</th>
												<th>Total Venta</th>
											</tr>
										</thead>
										<tbody>
											@foreach($ingDetHoy_var as $ingDetHoy)
											<tr>
												<td>{{$ingDetHoy->num_comprobante}}</td>
												<td>{{$ingDetHoy->idventa}}</td>
												<td>{{$ingDetHoy->nombre}}</td>
												<td>{{$ingDetHoy->cantidad}}</td>
												<td>{{number_format($ingDetHoy->precio_venta,0)}}</td>
												<td>{{number_format($ingDetHoy->descuento,0)}}</td>
												<td>{{number_format($ingDetHoy->total_venta,0)}}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
									<a type="button" class="btn btn-outline pull-left" href="{{asset('ventas/venta')}}" >Ir a todas las ventas</a>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
				</div>

				<div class="small-box bg-green">
					<div class="inner">
						<h3>@isset ($ingMes_var)  {{ number_format($ingMes_var,0) }}  @endisset<sup style="font-size: 20px">$</sup></h3>
						<p>Ingresos</p>
						<p>@isset ($mes_var )  {{ $mes_var  }}  @endisset</p>
					</div>
					<div class="icon">
						<i class="ion ion-arrow-graph-up-left"></i>
					</div><a class="small-box-footer" data-toggle="modal" data-target="#modal-success-mes" href="#">Detalles<i class="fa fa-arrow-circle-right"></i></a>
					<div class="modal modal-success fade" id="modal-success-mes">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">Ingresos por productos de este mes</h4>
								</div>
								<div class="modal-body">
									<table class="table" id="example-modalIngMes">
										<thead>
											<tr>
												<th>N° Comprobante</th>
												<th>ID Venta</th>
												<th>Nombre</th>
												<th>Cantidad</th>
												<th>Precio Venta</th>
												<th>Descuento</th>
												<th>Total Venta</th>
											</tr>
										</thead>
										<tbody>
											@foreach($ingDetMes_var as $ingDetMes)
											<tr>
												<td>{{$ingDetMes->num_comprobante}}</td>
												<td>{{$ingDetMes->idventa}}</td>
												<td>{{$ingDetMes->nombre}}</td>
												<td>{{$ingDetMes->cantidad}}</td>
												<td>{{number_format($ingDetMes->precio_venta,0)}}</td>
												<td>{{number_format($ingDetMes->descuento,0)}}</td>
												<td>{{number_format($ingDetMes->total_venta,0)}}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
									<a type="button" class="btn btn-outline pull-left" href="{{asset('ventas/venta')}}" >Ir a todas las ventas</a>

								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
				</div>

			</div><!-- ./col -->
			<div class="col-lg-3 col-md-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>@isset ($artHoy_var)  {{ number_format($artHoy_var,0) }}  @endisset</h3>
						<p>Articulos vendidos</p>
						<p>@isset ($hoy_var)  {{ $hoy_var }}  @endisset</p>
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div><a class="small-box-footer" data-toggle="modal" data-target="#modal-info" href="#">Detalle <i class="fa fa-arrow-circle-right"></i></a> 
					<div class="modal modal-info fade" id="modal-info"> 
						<div class="modal-dialog modal-lg"> 
						<div class="modal-content"> 
							<div class="modal-header"> 
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> 
							<h4 class="modal-title">Articulos vendidos hoy</h4> 
							</div> 
							<div class="modal-body"> 
							<table  class="table" id="ModalArticulos"> 
								<thead> 
								<tr> 
									<th >Numero comprobante</th> 
									<th >ID venta</th> 
									<th >ID Producto</th> 
									<th>Nombre categoria</th> 
									<th>Nombre producto</th> 
									<th>Cantidad</th> 
									
								</tr> 
								</thead> 
								<tbody> 
								@foreach($ventasArticulosHoy_var as $ventasArticulosHoy) 
								<tr> 
									<td>{{$ventasArticulosHoy->num_comprobante}}</td> 
									<td>{{$ventasArticulosHoy->idventa}}</td> 
									<td>{{$ventasArticulosHoy->id}}</td> 
									<td>{{$ventasArticulosHoy->nombre_c}}</td> 
									<td>{{$ventasArticulosHoy->nombre_a}}</td> 
									<td>{{$ventasArticulosHoy->cantidad}}</td> 
								</tr> 
								@endforeach 
								</tbody> 
							</table> 
							</div> 
							<div class="modal-footer"> 
							<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button> 
							<a type="button" class="btn btn-outline pull-left" href="{{asset('ventas/venta')}}" >Ir a todas las ventas</a> 
							</div> 
						</div> 
						<!-- /.modal-content --> 
						</div> 
						<!-- /.modal-dialog --> 
					</div> 
					<!-- /.modal --> 
					</div> 

				<!-- small box -->
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3>@isset ($cosMes_var)  {{ number_format($cosMes_var,0) }}  @endisset<sup style="font-size: 20px">$</sup></h3>
						<p>Costos totales</p>
						<p>@isset ($mes_var )  {{ $mes_var  }}  @endisset</p>
					</div>
					<div class="icon">
						<i class="ion ion-arrow-graph-down-left"></i>
					</div><a class="small-box-footer" data-toggle="modal" data-target="#modal-warning" href="#">Detalle <i class="fa fa-arrow-circle-right"></i></a> 
					<div class="modal modal-warning fade" id="modal-warning"> 
					<div class="modal-dialog modal-lg"> 
					<div class="modal-content"> 
						<div class="modal-header"> 
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> 
						<h4 class="modal-title">Costos totales mensual</h4> 
						</div> 
						<div class="modal-body"> 
						<table  class="table" id="ModalCostos"> 
							<thead> 
							<tr> 
								<th >Numero comprobante</th> 
								<th >ID ingreso</th> 
								<th >Nombre Producto</th> 
								<th>Cantidad</th> 
								<th>Costo producto</th> 
								<th>Costo total</th> 
								
							</tr> 
							</thead> 
							<tbody> 
							@foreach($CostoArticulo_var as $CostoArticulo) 
							<tr> 
								<td>{{$CostoArticulo->num_comprobante}}</td> 
								<td>{{$CostoArticulo->id}}</td> 
								<td>{{$CostoArticulo->nombre}}</td> 
								<td>{{$CostoArticulo->cantidad}}</td> 
								<td>{{number_format($CostoArticulo->precio_compra,0)}}</td> 
								<td>{{number_format($CostoArticulo->total_compra,0)}}</td> 
							</tr> 
							@endforeach 
							</tbody> 
						</table> 
						</div> 
						<div class="modal-footer"> 
						<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button> 
						<a type="button" class="btn btn-outline pull-left" href="{{asset('compras/ingreso')}}" >Ir a todos los ingresos</a> 
						</div> 
					</div> 
					<!-- /.modal-content --> 
					</div> 
					<!-- /.modal-dialog --> 
				</div> 
				<!-- /.modal --> 
				</div>

			</div><!-- ./col -->

			<div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
				<!-- PRODUCT LIST -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Últimas compras realizadas</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
							<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<ul class="products-list product-list-in-box">
							@foreach($articulos_var as $art)
							<li class="item">
								<div class="product-img">
									<img src="{{asset('imagenes/articulos/'.$art->imagen)}}" alt="{{ $art->nombre }}" height="100px" width="100px" class="img-thumbnail">
								</div>
								<div class="product-info">
									<a href="javascript:void(0)" class="product-title">{{ $art->nombre }}
										<span class="label label-warning pull-right">{{ $art->cantidad }}</span></a>
									<span class="product-description">
												{{ $art->categoria }} - {{ $art->codigo }} - {{ $art->nombre }}
											</span>
									<span class="product-description">
												Fecha de compra: {{ $art->fecha_hora }}
											</span>		
								</div>
							</li>
							@endforeach
						</ul>
					</div>
					<!-- /.box-body -->
					<div class="box-footer text-center">
						<a href="{{asset('compras/ingreso')}}" class="uppercase">View All Products</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col6 -->
    </div>
        <!-- /.row -->

		<div class="row">
			<div class="col-sm-6">
				<!-- DONUT CHART 
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Productos vendidos a la fecha (Feb. 9, 2018) </h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <canvas id="pieChart" style="height:250px"></canvas>
          </div>
           /.box-body  -->
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Densidad de ventas a la fecha (@isset ($hoy_var)  {{ $hoy_var }}  @endisset)</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" type="button"><i class="fa fa-minus"></i></button> <button class="btn btn-box-tool" data-widget="remove" type="button"><i class="fa fa-times"></i></button>
						</div>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-8">
								<div class="chart-responsive">
									<canvas height="210" id="pieChart"></canvas>
								</div><!-- ./chart-responsive -->
							</div><!-- /.col -->
							<div class="col-md-4">
								
								<p>TOP 5</p>
								<ul class="chart-legend clearfix">
									@foreach($pieDensidadVentas_var as $densidadVentas)
									<li><i class="fa fa-circle-o text-gray"></i> {{$densidadVentas->nombre}} </li>
									@endforeach
								</ul>
								<a href="#densidadVentas" title="Ir Arriba">Ir a la tabla de ventas</a>
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div><!-- /.box -->
			<div class="col-sm-6">
				<!-- DONUT CHART 
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Stock Inicial (Feb. 1, 2018)</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <canvas id="pieChart2" style="height:250px"></canvas>
          </div>
            /.box-body 
        </div>
         /.box -->
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Lista de stock inicial (1,@isset ($mes_var )  {{ $mes_var  }}  @endisset)</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" type="button"><i class="fa fa-minus"></i></button> <button class="btn btn-box-tool" data-widget="remove" type="button"><i class="fa fa-times"></i></button>
						</div>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-8">
								<div class="chart-responsive">
									<canvas height="210" id="pieChart2"></canvas>
								</div><!-- ./chart-responsive -->
							</div><!-- /.col -->
							<div class="col-md-4">
								<p>TOP 5</p>
								<ul class="chart-legend clearfix">
									@foreach($pieStockInicial5_var as $stockInicial)
									<li><i class="fa fa-circle-o text-gray"></i> {{$stockInicial->nombre}} </li>
									@endforeach
								</ul>
								<a href="#stockInicial" title="Ir Arriba">Ir a la tabla de detalles</a>
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="box">
					<div class="box-header">
						<a name="densidadVentas"><h3 class="box-title">Densidad de ventas a la fecha (@isset ($hoy_var)  {{ $hoy_var }}  @endisset)</h3></a>
					</div><!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered table-striped" id="example4">
							<thead>
								<tr>
									<th>ID Producto</th>
									<th>Categoria</th>
									<th>Marca</th>
									<th>Nombre</th>
									<th>Vendidos</th>
								</tr>
							</thead>
							<tbody>
								@foreach($tablaDensidadVentas_var as $densidadVentas)
								<tr>
									<td>{{$densidadVentas->id}}</td>
									<td>{{$densidadVentas->nombre_cat}}</td>
									<td>{{$densidadVentas->codigo}}</td>
									<td>{{$densidadVentas->nombre}}</td>
									<td>{{$densidadVentas->ventas}}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>ID Producto</th>
									<th>Categoria</th>
									<th>Marca</th>
									<th>Nombre</th>
									<th>Stock</th>
								</tr>
							</tfoot>
						</table>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
			<div class="col-sm-12">
				<div class="box">
					<div class="box-header">
						<a name="stockInicial"><h3 class="box-title">Lista de stock inicial (1,@isset ($mes_var )  {{ $mes_var  }}  @endisset)</h3></a>
					</div><!-- /.box-header -->
					<div class="box-body">
						<table class="table table-striped table-bordered dt-responsive" id="example3">
							<thead>
								<tr>
									<th>ID Producto</th>
									<th>Categoria</th>
									<th>Marca</th>
									<th>Nombre</th>
									<th>Stock</th>
								</tr>
							</thead>
							<tbody>
								@foreach($tablaStockInicial_var as $stockInicial)
								<tr>
									<td>{{$stockInicial->id}}</td>
									<td>{{$stockInicial->nombre_cat}}</td>
									<td>{{$stockInicial->codigo}}</td>
									<td>{{$stockInicial->nombre}}</td>
									<td>{{$stockInicial->cantidad}}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>ID Producto</th>
									<th>Categoria</th>
									<th>Marca</th>
									<th>Nombre</th>
									<th>Stock</th>
								</tr>
							</tfoot>
						</table>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div> <!-- /.COLUMNA -->
		</div> <!-- /.ROW -->
  

	</section>@endsection
</body>
</html>