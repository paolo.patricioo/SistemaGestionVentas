<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('almacen/categoria','CategoriaController');
Route::resource('almacen/articulo','ArticuloController');
Route::resource('ventas/cliente','ClienteController');
Route::resource('compras/proveedor','ProveedorController');
Route::resource('compras/ingreso','IngresoController');
Route::resource('ventas/venta','VentaController');
Route::resource('seguridad/usuario','UsuarioController');



Route::get('/', function () {
    return view('auth/login');
}); 

Route::get('/dashboard', 'dashboardController@index');
Route::get('/home', 'dashboardController@index');

Route::get('logout', 'Auth\LoginController@logout');

/* Route::get('test/1', function ()
{
    $bla=DB::table('meses as m')
        ->leftJoin('ingreso as i','m.num_mes','=','i.mes_ingreso')
        ->leftJoin('detalle_ingreso as di','i.id','=','di.idingreso')
        ->select('m.mestxt as mes',DB::raw('sum(di.cantidad * di.precio_compra) as compra'))
        ->groupby('m.mestxt')
        ->orderby('m.idmeses')
        ->get();
    return $bla;                        
}); */

Auth::routes();

