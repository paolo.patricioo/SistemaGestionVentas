<?php

namespace SistemaGestionVentas;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
     protected $table='articulo';
    protected $primarykey='id';
    public $timestamps=false;


    protected $fillable =[
        'idcategoria',
        'codigo',
        'nombre',
        'stock',
        'descripcion',
        'imagen',
        'estado'
    ];
    protected $guarded =[

    ];
}
